#!/bin/sh -ex
# Copyright 2023 Oliver Smith
# SPDX-License-Identifier: GPL-3.0-or-later

# Shell commands running after the installation is complete. This gets called
# by shellprocess.conf, as written by ondev-prepare.sh.

CRYPTDEV_CALAMARES=/dev/mapper/calamares_crypt

is_encrypted() {
	test -e "$CRYPTDEV_CALAMARES"
}

get_uuid() {
	blkid -s UUID -o value "$1"
}

create_crypttab() {
	local crypttab=/mnt/install/etc/crypttab
	local luks_uuid="$(get_uuid "$CRYPTDEV_CALAMARES")"

	echo "root UUID=$luks_uuid none luks" >"$crypttab"

	cat "$crypttab"
}

create_fstab() {
	local fstab=/mnt/install/etc/fstab
	local root_filesystem="$(findmnt -n -o FSTYPE --target /mnt/install)"
	local root_mount_point="/dev/mapper/root"

	if ! is_encrypted; then
		local root_dev="$(findmnt -n -o SOURCE --target /mnt/install)"
		root_mount_point="UUID=$(get_uuid "$root_dev")"
	fi

	# Figuring out the UUID of the boot partition here would be quite
	# complex (depends on whether installed from external storage to
	# internal; if not, it depends on what the exact partition number for
	# boot is, it might be different on e.g. chromebooks). Do it by label
	# for now.
	local boot_mount_point="LABEL=pmOS_boot"
	local boot_filesystem="auto"

	{ echo "# <file system> <mount point> <type> <options> <dump> <pass>"
	  echo "$root_mount_point / $root_filesystem defaults 0 0"
	  echo "$boot_mount_point /boot $boot_filesystem defaults 0 0"
	} >"$fstab"

	cat "$fstab"
}

# Create /etc/fstab and /etc/crypttab, like in pmbootstrap:
# https://git.sr.ht/~postmarketos/pmbootstrap/tree/2.0.0/item/pmb/install/_install.py#L799
update_fstab_crypttab() {
	create_fstab
	if is_encrypted; then
		create_crypttab
	fi
}

. /usr/share/misc/source_deviceinfo

update_fstab_crypttab

if [ -e /tmp/ondev-internal-storage ]; then
	# External to internal storage
	
	# just poweroff, so the user can pull out the SD card before next boot.
	# It would be nice if we could show a "finished" screen instead of
	# doing this right after the install is done without further notice.
	# See: https://github.com/calamares/calamares/issues/1601
	poweroff
else
	# Installed to same storage as the installation medium (e.g. SD to SD,
	# or eMMC to eMMC in factory image)

	# Rename install partition, so the initramfs will boot into the new
	# pmOS_root partition instead (unless installed from external to
	# internal storage)
	tune2fs -L pmOS_deleteme /dev/disk/by-label/pmOS_install

	# Rename the installer's boot partition, in case the on-device
	# installer image was flashed to eMMC and installed there (like in
	# the factory). If the user plugs an SD card with the on-device
	# installer afterwards, we want that installer to prefer its own
	# pmOS_inst_boot partition over the pmOS_boot partition on the
	# eMMC. ("pmOS_i_boot" fits 11 chars for fat32; "pmOS_inst_boot" is for
	# backwards compatibility with older pmbootstrap versions)
	for part in pmOS_i_boot pmOS_inst_boot; do
		part_path=/dev/disk/by-label/"$part"
		if [ ! -e "$part_path" ]; then
			continue
		fi
		case $deviceinfo_boot_filesystem in
			ext2|ext3|ext4|'') tune2fs -L pmOS_boot "$part_path" ;;
			fat16|fat32|fat64) fatlabel "$part_path" pmOS_boot ;;
			*)
				echo "Failed to rename $part: " \
					"No suitable tool for " \
					"$deviceinfo_boot_filesystem"
				exit 1
				;;
		esac
	done

	reboot
fi
