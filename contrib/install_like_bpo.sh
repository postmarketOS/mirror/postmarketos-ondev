#!/bin/sh -e
# Copyright 2023 Oliver Smith
# SPDX-License-Identifier: GPL-3.0-or-later

# Create an installation image with pmbootstrap like bpo does it, with
# optionally building calamares, calamares-extensions, tinydm,
# postmarketos-ondev from source. When done, run it in qemu.

echo "NOTE: this does not run zap"
set -x
sudo true

BUILD_CALA=0
BUILD_CALX=0
BUILD_ONDEV=1
BUILD_TINYDM=0
# PMB_QEMU_ARGS="--host-qemu --display=gtk"
DISABLE_SPLASH=1

UI=console

fork_pkg() {
	if [ ! -f temp/"$1"/APKBUILD ]; then
		pmbootstrap -y aportgen --fork-alpine "$1"
	fi
}

cd ~/code/pmbootstrap/aports

# Disable splash
CMDLINE="$(. device/main/device-qemu-amd64/deviceinfo && echo "$deviceinfo_kernel_cmdline")"
test -n "$CMDLINE"
if [ "$DISABLE_SPLASH" = 1 ]; then
	CMDLINE="$CMDLINE PMOS_NOSPLASH"
fi

if [ "$BUILD_CALA" = 1 ]; then
	fork_pkg calamares
	pmbootstrap --details-to-stdout \
		build \
			--src=~/code/calamares \
			calamares
fi

if [ "$BUILD_CALX" = 1 ]; then
	fork_pkg calamares-extensions
	pmbootstrap --details-to-stdout \
		build \
			--src=~/code/calamares-extensions \
			calamares-extensions
fi

if [ "$BUILD_ONDEV" = 1 ]; then
	pmbootstrap --details-to-stdout \
		build \
			--src=~/code/postmarketos-ondev \
			postmarketos-ondev
fi

if [ "$BUILD_TINYDM" = 1 ]; then
	fork_pkg tinydm
	pmbootstrap --details-to-stdout \
		build \
			--src=~/code/tinydm \
			tinydm
fi

pmbootstrap config device qemu-amd64
pmbootstrap config ui "$UI"

pmbootstrap config boot_size 128
pmbootstrap config kernel virt

pmbootstrap config extra_space 0
pmbootstrap config extra_packages unl0kr

IMG_DATE="$(date +%Y%m%d-%H%M)"
IMG_PREFIX="$IMG_DATE"-postmarketOS-qemu-amd64-test

if ! [ -e /tmp/out/$IMG_PREFIX.img ]; then
	yes | pmbootstrap \
		--details-to-stdout \
		install \
		--no-sshd \
		| tee /tmp/log_img

	mkdir -p /tmp/out
	sudo mv $(pmbootstrap config work)/chroot_native/home/pmos/rootfs/qemu-amd64.img \
		/tmp/out/$IMG_PREFIX.img
fi

pmbootstrap config extra_space 100
pmbootstrap config extra_packages none

sudo mkdir -p $(pmbootstrap config work)/chroot_installer_qemu-amd64/var/lib
sudo cp /tmp/out/$IMG_PREFIX.img \
	$(pmbootstrap config work)/chroot_installer_qemu-amd64/var/lib/rootfs.img

rm -f /tmp/log_img_installer
yes | pmbootstrap \
	--details-to-stdout \
	install \
	--ondev \
	--no-sshd \
	| tee /tmp/log_img_installer

export DISPLAY=:0
pmbootstrap qemu \
	--image-size=8G \
	$PMB_QEMU_ARGS \
	--second-storage \
	--cmdline "$CMDLINE"
