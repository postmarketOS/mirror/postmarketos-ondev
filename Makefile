# Copyright 2020 Oliver Smith
# SPDX-License-Identifier: GPL-3.0-or-later
DESTDIR :=

# Nothing to compile
default:
	$(info usage: 'make DESTDIR="..." install')

install:
	cd config && for cfg in settings.conf modules/*.conf; do \
		install -Dm644 "$$cfg" \
			"$(DESTDIR)/etc/calamares/$$cfg" || exit 1; \
	done

	for i in *.sh; do \
		install -Dm755 "$$i" \
			"$(DESTDIR)/usr/bin/$$(echo "$$i" | sed 's/\.sh$$//')"; \
	done

	install -Dm644 logo.png \
		"$(DESTDIR)/usr/share/calamares/branding/default-mobile/logo.png"
	install -Dm755 postmarketos-ondev.initd \
		"$(DESTDIR)/etc/init.d/postmarketos-ondev"

.PHONY: default install
